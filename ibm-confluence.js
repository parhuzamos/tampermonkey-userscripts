// ==UserScript==
// @name         IBM CONFIDENTIAL remover for Confluence
// @namespace    http://parhuzamos.com/
// @version      0.1
// @description  Remove the announcement-banner at the bottom if text contains only IBM CONFIDENTIAL stuff
// @author       parhuzamos
// @match        https://wiki.ustream-adm.in/*
// @grant        none
// @updateUrl    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/ibm-confluence.js
// ==/UserScript==

(function() {
    'use strict';

    //window.setTimeout(function() {
        var e = document.querySelector(".aui-message");
        if ((e != null) && (e.textContent.trim().startsWith("IBM CONFIDENTIAL - FOR AUTHORIZED PERSONNEL ONLY"))) {
            e.style.display = 'none';
        }
    //}, 0);

})();