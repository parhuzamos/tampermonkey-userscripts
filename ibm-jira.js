// ==UserScript==
// @name         IBM CONFIDENTIAL remover for Jira
// @namespace    http://parhuzamos.com/
// @version      0.1
// @description  Remove the announcement-banner if text contains only IBM CONFIDENTIAL stuff
// @author       parhuzamos
// @match        https://issues.ustream-adm.in/*
// @grant        none
// @updateUrl    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/ibm-jira.js
// ==/UserScript==

(function() {
    'use strict';

    //window.setTimeout(function() {
        var e = document.querySelector("#announcement-banner.alertHeader");
        if ((e != null) && (e.textContent.trim() == "IBM CONFIDENTIAL - FOR AUTHORIZED PERSONNEL ONLY || IBM's internal systems must only be used for conducting IBM's business or for purposes authorized by IBM management")) {
            e.style.display = 'none';
        }
    //}, 0);
})();