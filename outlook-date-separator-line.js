// ==UserScript==
// @name     Outlook date separator line
// @version      0.1
// @description  Black line after each day
// @match    https://outlook.office.com/mail/*
// @grant    GM_addStyle
// @run-at   document-start
// @updateURL    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/outlook-date-separator-line.js
// @editURL      https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/edit/master/outlook-date-separator-line.js
// ==/UserScript==

GM_addStyle ( `
    ._2mO5hYqEYsxNL3pgqArOzJ {
        border-top: 5px solid black;
    }
` );
