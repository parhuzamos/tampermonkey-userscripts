// ==UserScript==
// @name         M$ Teams - Teams (posts) opener
// @namespace    http://tampermonkey.net/
// @version      2024-04-24
// @description  if Teams opened with the url below, the Teams (posts) button is clicked
// @author       parhuzamos
// @match        https://teams.microsoft.com/v2/?target=posts
// @icon         https://www.google.com/s2/favicons?sz=64&domain=microsoft.com
// @grant        none
// @updateUrl    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/teams-posts-opener.js
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...
    function checkElement() {
        console.log("Checking for Teams button...");
        const e = document.getElementById("2a84919f-59d8-4441-a975-2a8c2643b741");
        if (e != null) {
            console.log("Button found, clicking and exiting...");
            e.click()
        } else {
            window.setTimeout(checkElement, 1000)
        }
    }

    window.setTimeout(checkElement, 1000)
})();