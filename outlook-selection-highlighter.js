// ==UserScript==
// @name         Outlook selection highlight
// @namespace    http://tampermonkey.net/
// @version      v0.1
// @description  Set the selected mail's background to gold
// @author       @parhuzamos
// @match        https://outlook.office.com/mail/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=office.com
// @grant        none
// @updateURL    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/outlook-selection-highlighter.js
// @editURL      https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/edit/master/outlook-selection-highlighter.js
// ==/UserScript==

(function() {
    'use strict';

    const css = '#MailList div[aria-selected=true] .lHRXq { background: gold; border: 2px solid darkcyan; }';
    const head = document.getElementsByTagName('head')[0];
    const style = document.createElement('style');

    style.type = 'text/css';
    if (style.styleSheet){
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);
})();