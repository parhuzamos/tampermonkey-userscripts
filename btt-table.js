// ==UserScript==
// @name         BTT Tábla header
// @namespace    http://tampermonkey.net/
// @version      2024-03-28
// @description  add extra text to the header
// @author       parhuzamos
// @match        https://binx.atlassian.net/jira/software/c/projects/BTT/boards/65
// @icon         https://www.google.com/s2/favicons?sz=64&domain=atlassian.net
// @grant        none
// @updateUrl    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/btt-table.js
// ==/UserScript==

(function() {
    'use strict';

    window.setTimeout(function() {
        console.log("***** doing work")
        jQuery("[data-testid='software-board.header.title.container']")[0].style["max-width"] = "1024px"
        jQuery("[data-testid='software-board.header.title.container'] h1")[0].innerText = "BTT Tábla - Summary: Home Office igénylés - Albertini Bence - {hónap}"
        console.log("***** done")
    }, 3000)

})();