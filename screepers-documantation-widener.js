// ==UserScript==
// @name         Screepers documentation widener
// @namespace    http://parhuzamos.com/
// @version      0.1
// @description  Remove table of contents from the right and make the main part wider
// @author       parhuzamos
// @match        https://docs.screeps.com/
// @updateUrl    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/screepers-documantation-widener.js
// @icon         https://www.google.com/s2/favicons?sz=64&domain=screeps.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    var e = document.querySelector("#article-toc");
    if (e != null) {
        e.style.display = 'none';
        var f = document.querySelector(".article-inner");
        if (f != null) {
            f.style.marginRight = 0;
        }
    }
})();
