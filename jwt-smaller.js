// ==UserScript==
// @name         smaller jwt.io
// @namespace    http://tampermonkey.net/
// @version      2024-02-26
// @description  Removes unused portions of the page
// @author       parhuzamos
// @match        https://jwt.io/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=jwt.io
// @grant        none
// @updateUrl    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/jwt-smaller.js
// ==/UserScript==

(function() {
    'use strict';

    document.getElementsByClassName("banner-jwt").item(0).style.display = "none";
    document.getElementsByClassName("top-banner-bg").item(0).style.display = "none";
    document.getElementsByClassName("top-banner-spacer").item(0).style.display = "none";
    document.getElementsByClassName("top-banner").item(0).style.display = "none";
    document.getElementsByClassName("top-banner").item(0).style.display = "none";
    document.getElementsByClassName("navbar").item(0).style.display = "none";
    document.getElementsByClassName("container").item(4).style.display = "none";
    document.getElementsByClassName("sources").item(0).style.display = "none";
    document.getElementsByClassName("tokens-created").item(0).style.display = "none";
    document.getElementsByTagName("footer").item(0).style.display = "none";

    document.getElementById("debugger-io").style.padding = 0;
    document.getElementById("debugger-io").getElementsByClassName("jwt-playground").item(0).getElementsByClassName("warning").item(0).style.display = "none";
    document.getElementById("debugger-io").getElementsByClassName("container").item(0).getElementsByTagName("H1").item(0).style.display = "none";

    // Your code here...
})()