// ==UserScript==
// @name         Turistautak panel resize
// @namespace    http://parhuzamos.com/
// @version      0.5
// @description  Resize left to 12%, right to 88%.
// @author       parhuzamos
// @match        https://turistautak.openstreetmap.hu/*
// @grant        none
// @updateURL    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/turistautak.js
// @editURL      https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/edit/master/turistautak.js
// ==/UserScript==

(function() {
    'use strict';

    document.querySelector("#bal").style.width="12%";
    document.querySelector("#jobb").style.width="88%";
    document.querySelector("[onclick='torles();']").style.backgroundColor="#E26044";
    document.querySelector("input[value='nebt']").click();
    document.querySelector("#gt").style.backgroundColor="#71D67F";
    document.querySelector("#gt").value="Útvonaltervezés (F9)";
    document.addEventListener('keydown', catchKey);

    function catchKey(e) {
        if (e.code == "F9") {
            var b = map.getBounds();

            ega();
            utterv();

            window.setTimeout(function() {map.fitBounds(b)}, 1500);
        }
    }

    map.fitBounds(new L.LatLngBounds({lat: 47.56117948819046, lng: 19.070892333984375}, {lat: 47.52502570956561, lng: 18.906097412109375}));
})();
