// ==UserScript==
// @name         Outlook logout recdirect
// @version      0.1
// @description  Redirect to login after automatic(?) logout
// @author       parhuzamos
// @match        https://login.microsoftonline.com/*
// @grant        none
// @updateURL    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/outlook-logout-redirect.js
// @editURL      https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/edit/master/outlook-logout-redirect.js
// ==/UserScript==

(function() {
    'use strict';

    if (document.location.href == "https://login.microsoftonline.com/common/oauth2/logout") {
        document.location.href = "https://outlook.office.com/mail/";
    }
})();
