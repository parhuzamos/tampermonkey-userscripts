// ==UserScript==
// @name         Teams App Selector
// @namespace    http://tampermonkey.net/
// @version      2024-02-29
// @description  try to take over the world!
// @author       parhuzamos
// @match        https://teams.microsoft.com/v2/?my-custom-goto=*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=microsoft.com
// @updateUrl    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/teams-app-selector.js
// @grant        none
// ==/UserScript==


(function() {
    'use strict';

    function doSearch() {
        console.log("****searching*****");
        var e = document.getElementById("ef56c0de-36fc-4ef8-b417-3d82ba9d073c");
        if (e) {
            const target = new URLSearchParams(window.location.search).get('my-custom-goto');

            if (target === "calendar") {
                console.log("*****clicking calendar")
                document.getElementById("ef56c0de-36fc-4ef8-b417-3d82ba9d073c").click();
            } else if (target === "teams") {
                console.log("*****clicking teams")
                document.getElementById("2a84919f-59d8-4441-a975-2a8c2643b741").click();
            } else if (target === "chat") {
                console.log("*****clicking chat")
                document.getElementById("86fcd49b-61a2-4701-b771-54728cd291fb").click();
            }
        } else {
            window.setTimeout(doSearch, 500);
        }
    }

    doSearch();
})();