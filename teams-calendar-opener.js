// ==UserScript==
// @name         M$ Teams - Calendar opener
// @namespace    http://tampermonkey.net/
// @version      2024-04-24
// @description  if Teams opened with the url below, the Calendar button is clicked
// @author       parhuzamos
// @match        https://teams.microsoft.com/v2/?target=calendar
// @icon         https://www.google.com/s2/favicons?sz=64&domain=microsoft.com
// @grant        none
// @updateUrl    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/teams-calendar-opener.js
// ==/UserScript==

(function() {
    'use strict';

    function checkElement() {
        console.log("Checking for Calendar button...");
        const e = document.getElementById("ef56c0de-36fc-4ef8-b417-3d82ba9d073c");
        if (e != null) {
            console.log("Button found, clicking and exiting...");
            e.click()
        } else {
            window.setTimeout(checkElement, 1000)
        }
    }

    window.setTimeout(checkElement, 1000)
})();