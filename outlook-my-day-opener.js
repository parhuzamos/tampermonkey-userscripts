// ==UserScript==
// @name         Outlook My Day openeder
// @version      0.1
// @description  Open the My Day right side
// @author       parhuzamos
// @match        https://outlook.office.com/mail/*
// @grant        none
// @updateURL    https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/raw/master/outlook-my-day-opener.js
// @editURL      https://gitlab.com/parhuzamos/tampermonkey-userscripts/-/edit/master/outlook-my-day-opener.js
// ==/UserScript==

(function() {
    'use strict';

    setTimeout(() => {
        document.querySelector("button[aria-label='My Day']").click();
    }, 4000);
})();
